package ru.t1.malyugin.tm.util;

import org.apache.commons.lang3.math.NumberUtils;

import java.util.Scanner;

public interface TerminalUtil {

    Scanner SCANNER = new Scanner(System.in);

    static String nextLine() {
        return SCANNER.nextLine();
    }

    static Integer nextInteger() {
        final String value = nextLine();
        return Integer.parseInt(value);
    }

    static Integer nextIntegerSafe() {
        final String value = nextLine();
        if (NumberUtils.isParsable(value)) return Integer.parseInt(value);
        return null;
    }

}