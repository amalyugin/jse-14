package ru.t1.malyugin.tm.controller;

import ru.t1.malyugin.tm.api.controller.ITaskController;
import ru.t1.malyugin.tm.api.service.ITaskService;
import ru.t1.malyugin.tm.enumerated.Sort;
import ru.t1.malyugin.tm.enumerated.Status;
import ru.t1.malyugin.tm.model.Task;
import ru.t1.malyugin.tm.util.TerminalUtil;

import java.util.List;

public final class TaskController implements ITaskController {

    private final ITaskService taskService;

    public TaskController(final ITaskService taskService) {
        this.taskService = taskService;
    }

    @Override
    public void showTasks() {
        System.out.println("[SHOW TASKS]");

        System.out.print("ENTER SORT: ");
        System.out.println(Sort.renderValuesList());
        final Integer sortIndex = TerminalUtil.nextIntegerSafe();
        final Sort sort = Sort.getSortByIndex(sortIndex);
        final List<Task> tasks = taskService.findAll(sort);
        if (tasks.isEmpty()) {
            System.out.println("[NO RECORDS]");
            return;
        }
        renderTaskList(tasks);

        System.out.println("[OK]");
    }

    @Override
    public void showTasksByProjectId() {
        System.out.println("[SHOW TASKS BY PROJECT ID]");

        System.out.print("ENTER PROJECT ID: ");
        final String projectId = TerminalUtil.nextLine();
        final List<Task> tasks = taskService.findAllByProjectId(projectId);
        if (tasks == null || tasks.isEmpty()) {
            System.out.println("[NO RECORDS]");
            return;
        }
        renderTaskList(tasks);

        System.out.println("[OK]");
    }

    @Override
    public void showTaskById() {
        System.out.println("[SHOW TASKS BY ID]");

        System.out.print("ENTER ID: ");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.findOneById(id);
        if (task == null) {
            System.out.println("[TASK NOT FOUND]");
            return;
        }
        System.out.println(task);
        System.out.println("[OK]");
    }

    @Override
    public void showTaskByIndex() {
        System.out.println("[SHOW TASKS BY INDEX]");

        System.out.print("ENTER INDEX: ");
        final Integer index = TerminalUtil.nextInteger() - 1;
        final Task task = taskService.findOneByIndex(index);
        if (task == null) {
            System.out.println("[TASK NOT FOUND]");
            return;
        }
        System.out.println(task);
        System.out.println("[OK]");
    }

    @Override
    public void createTask() {
        System.out.println("[CREATE TASK]");

        System.out.print("ENTER NAME: ");
        final String name = TerminalUtil.nextLine();
        System.out.print("ENTER DESCRIPTION: ");
        final String description = TerminalUtil.nextLine();
        final Task task = taskService.create(name, description);
        if (task == null) {
            System.out.println("[FAILED]");
            return;
        }

        System.out.println("NEW TASK - " + task.toString());
        System.out.println("[OK]");
    }

    @Override
    public void clearTasks() {
        System.out.println("[REMOVE TASKS]");
        taskService.clear();
        System.out.println("[OK]");
    }

    @Override
    public void removeTaskById() {
        System.out.println("[REMOVE TASK BY ID]");

        System.out.print("ENTER ID: ");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.removeById(id);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[OK]");
    }

    @Override
    public void removeTaskByIndex() {
        System.out.println("[REMOVE TASK BY INDEX]");

        System.out.print("ENTER INDEX: ");
        final Integer index = TerminalUtil.nextInteger() - 1;
        final Task task = taskService.removeByIndex(index);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[OK]");
    }

    @Override
    public void updateTaskById() {
        System.out.println("[UPDATE TASK BY ID]");

        System.out.print("ENTER ID: ");
        final String id = TerminalUtil.nextLine();
        System.out.print("ENTER NEW NAME: ");
        final String name = TerminalUtil.nextLine();
        System.out.print("ENTER NEW DESCRIPTION: ");
        final String description = TerminalUtil.nextLine();

        final Task task = taskService.updateById(id, name, description);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.printf("TASK %s - UPDATED%n", id);
        System.out.println("[OK]");
    }

    @Override
    public void updateTaskByIndex() {
        System.out.println("[UPDATE TASK BY INDEX]");

        System.out.print("ENTER INDEX: ");
        final Integer index = TerminalUtil.nextInteger() - 1;
        System.out.print("ENTER NEW NAME: ");
        final String name = TerminalUtil.nextLine();
        System.out.print("ENTER NEW DESCRIPTION: ");
        final String description = TerminalUtil.nextLine();

        final Task task = taskService.updateByIndex(index, name, description);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.printf("TASK %s UPDATED", task.getId());
        System.out.println("[OK]");
    }

    @Override
    public void startTaskById() {
        System.out.println("[START TASK BY ID]");

        System.out.print("ENTER ID: ");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.changeTaskStatusById(id, Status.IN_PROGRESS);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.printf("TASK %s - STARTED%n", id);
        System.out.println("[OK]");
    }

    @Override
    public void startTaskByIndex() {
        System.out.println("[START TASK BY INDEX]");

        System.out.print("ENTER INDEX: ");
        final Integer index = TerminalUtil.nextInteger() - 1;
        final Task task = taskService.changeTaskStatusByIndex(index, Status.IN_PROGRESS);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.printf("TASK %s - STARTED%n", task.getId());
        System.out.println("[OK]");
    }

    @Override
    public void completeTaskById() {
        System.out.println("[COMPLETE TASK BY ID]");

        System.out.print("ENTER ID: ");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.changeTaskStatusById(id, Status.COMPLETED);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.printf("TASK %s - COMPLETED%n", id);
        System.out.println("[OK]");
    }

    @Override
    public void completeTaskByIndex() {
        System.out.println("[COMPLETE TASK BY INDEX]");

        System.out.print("ENTER INDEX: ");
        final Integer index = TerminalUtil.nextInteger() - 1;
        final Task task = taskService.changeTaskStatusByIndex(index, Status.COMPLETED);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.printf("TASK %s - COMPLETED%n", task.getId());
        System.out.println("[OK]");
    }

    @Override
    public void changeTaskStatusById() {
        System.out.println("[CHANGE TASK STATUS BY ID]");

        System.out.print("ENTER ID: ");
        final String id = TerminalUtil.nextLine();
        System.out.print("ENTER STATUS: ");
        System.out.println(Status.renderValuesList());
        final int statusIndex = TerminalUtil.nextInteger();
        if (statusIndex < 0 || statusIndex >= Status.values().length) {
            System.out.println("[FAIL]");
            return;
        }
        final Status status = Status.values()[statusIndex];
        final Task task = taskService.changeTaskStatusById(id, status);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.printf("TASK %s CHANGED STATUS TO %s%n", task.getId(), status.getDisplayName());
        System.out.println("[OK]");
    }

    @Override
    public void changeTaskStatusByIndex() {
        System.out.println("[CHANGE TASK STATUS BY INDEX]");

        System.out.print("ENTER INDEX: ");
        final Integer taskIndex = TerminalUtil.nextInteger() - 1;
        System.out.print("ENTER STATUS: ");
        System.out.println(Status.renderValuesList());
        final int statusIndex = TerminalUtil.nextInteger();
        if (statusIndex < 0 || statusIndex >= Status.values().length) {
            System.out.println("[FAIL]");
            return;
        }
        final Status status = Status.values()[statusIndex];
        final Task task = taskService.changeTaskStatusByIndex(taskIndex, status);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.printf("TASK %s CHANGED STATUS TO %s%n", task.getId(), status.getDisplayName());
        System.out.println("[OK]");
    }

    private void renderTaskList(final List<Task> tasks) {
        int index = 1;
        for (final Task task : tasks) {
            if (task == null) continue;
            System.out.println(index + ". " + task.toString());
            index++;
        }
    }

}