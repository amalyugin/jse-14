package ru.t1.malyugin.tm.controller;

import ru.t1.malyugin.tm.api.controller.IProjectTaskController;
import ru.t1.malyugin.tm.api.service.IProjectTaskService;
import ru.t1.malyugin.tm.model.Task;
import ru.t1.malyugin.tm.util.TerminalUtil;

public final class ProjectTaskController implements IProjectTaskController {

    public final IProjectTaskService projectTaskService;

    public ProjectTaskController(final IProjectTaskService projectTaskService) {
        this.projectTaskService = projectTaskService;
    }

    @Override
    public void bindTaskToProject() {
        System.out.println("[BIND TASK TO PROJECT]");
        System.out.print("ENTER PROJECT ID: ");
        final String projectId = TerminalUtil.nextLine();
        System.out.print("ENTER TASK ID: ");
        final String taskId = TerminalUtil.nextLine();
        final Task task = projectTaskService.bindTaskToProject(projectId, taskId);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[OK]");
    }

    @Override
    public void unbindTaskFromProject() {
        System.out.println("[UNBIND TASK TO PROJECT]");
        System.out.print("ENTER PROJECT ID: ");
        final String projectId = TerminalUtil.nextLine();
        System.out.print("ENTER TASK ID: ");
        final String taskId = TerminalUtil.nextLine();
        final Task task = projectTaskService.unbindTaskFromProject(projectId, taskId);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[OK]");
    }

}