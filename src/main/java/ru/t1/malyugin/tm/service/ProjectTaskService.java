package ru.t1.malyugin.tm.service;

import org.apache.commons.lang3.StringUtils;
import ru.t1.malyugin.tm.api.repository.IProjectRepository;
import ru.t1.malyugin.tm.api.repository.ITaskRepository;
import ru.t1.malyugin.tm.api.service.IProjectTaskService;
import ru.t1.malyugin.tm.model.Project;
import ru.t1.malyugin.tm.model.Task;

import java.util.List;

public final class ProjectTaskService implements IProjectTaskService {

    private final IProjectRepository projectRepository;

    private final ITaskRepository taskRepository;

    public ProjectTaskService(final IProjectRepository projectRepository, final ITaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @Override
    public Task bindTaskToProject(final String projectId, final String taskId) {
        if (StringUtils.isEmpty(projectId)) return null;
        if (StringUtils.isEmpty(taskId)) return null;
        if (projectRepository.findOneById(projectId) == null) return null;
        final Task task = taskRepository.findOneById(taskId);
        if (task == null) return null;
        task.setProjectId(projectId);
        return task;
    }

    @Override
    public Project removeProjectById(final String projectId) {
        if (StringUtils.isEmpty(projectId)) return null;
        if (projectRepository.findOneById(projectId) == null) return null;
        final List<Task> tasks = taskRepository.findAllByProjectId(projectId);
        for (final Task task : tasks) taskRepository.remove(task);
        return projectRepository.removeById(projectId);
    }

    @Override
    public Task unbindTaskFromProject(final String projectId, final String taskId) {
        if (StringUtils.isEmpty(projectId)) return null;
        if (StringUtils.isEmpty(taskId)) return null;
        if (projectRepository.findOneById(projectId) == null) return null;
        final Task task = taskRepository.findOneById(taskId);
        if (task == null) return null;
        task.setProjectId(null);
        return task;
    }

}