package ru.t1.malyugin.tm.api.controller;

public interface IProjectController {

    void showProjects();

    void showProjectById();

    void showProjectByIndex();

    void createProject();

    void clearProjects();

    void removeProjectById();

    void removeProjectByIndex();

    void updateProjectById();

    void updateProjectByIndex();

    void startProjectById();

    void startProjectByIndex();

    void completeProjectById();

    void completeProjectByIndex();

    void changeProjectStatusById();

    void changeProjectStatusByIndex();

}